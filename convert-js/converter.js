// Referencing https://hadrysmateusz.medium.com/using-files-in-client-side-javascript-applications-ultimate-guide-5ad54c98664b for file reading in js

$( document ).ready( function() {
  
  $( "#convert_ready" ).click( function() {
    var file = document.getElementById( "input_file" ).files[0];
    console.log( file );
    
    const reader = new FileReader();
    
    // Additional docs: https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsText
    reader.addEventListener( "load", () => {
      var jsonString = reader.result;
      
      var jsonData = JSON.parse( jsonString );
      
      var fields = 
      [ 
        { "field" : "day",                "type" : "single" },
        { "field" : "sleep",              "type" : "single" },
        { "field" : "period",             "type" : "single" },
        { "field" : "fluid",              "type" : "single" },
        { "field" : "injection",          "type" : "single" },
        { "field" : "iud",                "type" : "single" },
        { "field" : "pill",               "type" : "single" },
        { "field" : "patch",              "type" : "single" },
        { "field" : "ring",               "type" : "single" },
        { "field" : "pain",               "type" : "list" },
        { "field" : "sex",                "type" : "list" },
        { "field" : "birth_control",      "type" : "list" },
        { "field" : "energy",             "type" : "list" },
        { "field" : "mental",             "type" : "list" },
        { "field" : "mood",               "type" : "list" },
        { "field" : "motivation",         "type" : "list" },
        { "field" : "social",             "type" : "list" },
        { "field" : "ailment",            "type" : "list" },
        { "field" : "appointment",        "type" : "list" },
        { "field" : "collection_method",  "type" : "list" },
        { "field" : "craving",            "type" : "list" },
        { "field" : "digestion",          "type" : "list" },
        { "field" : "exercise",           "type" : "list" },
        { "field" : "hair",               "type" : "list" },
        { "field" : "medication",         "type" : "list" },
        { "field" : "party",              "type" : "list" },
        { "field" : "poop",               "type" : "list" },
        { "field" : "skin",               "type" : "list" },
        { "field" : "tags",               "type" : "list" },
        { "field" : "test",               "type" : "list" },
        { "field" : "bbt",                "type" : "dict" },
        { "field" : "weight",             "type" : "dict" },
      ];
      
      // Convert into CSV format
      var csv = "";
      
      // HEADER
      for ( var j = 0; j < fields.length; j++ )
      {
        if ( j != 0 ) { csv += ","; }
        csv += fields[j]["field"];
      }
      csv += "\n";
        
      // DATA
      for ( var record = 0; record < jsonData["data"].length; record++ )
      {
        //console.log( "Record:", jsonData["data"][record] );
        
        for ( var f = 0; f < fields.length; f++ )
        {          
          //console.log( "Field:", fields[f]["field"] );
          if ( f != 0 ) { csv += ","; }
          if ( fields[f]["field"] in jsonData["data"][record] )
          {
            var recordData = jsonData["data"][record][ fields[f]["field"] ];
            
            if ( fields[f]["type"] == "single" )
            {
              csv += recordData;
            }
            else if ( fields[f]["type"] == "list" )
            {
              var list = "";
              for ( var i = 0; i < recordData.length; i++ )
              {
                if ( i != 0 ) { list += "/"; }
                list += recordData[i];
              }
              csv += list;
            }
            else if ( fields[f]["type"] == "dict" )
            {
              var dictInfo = "";
              
              for ( var key in recordData )
              {
                if ( dictInfo != "" ) { dictInfo += "/"; }
                dictInfo += key + "=" + recordData[key];
              }
              
              csv += dictInfo;
            }
          }
        }
        
        csv += "\n";
      }
      
      console.log( "CSV:", csv );
      
      // Referenced: https://www.codegrepper.com/code-examples/javascript/create+save+file+javascript
      var blobOutput = new Blob( [csv], {type: "csv"} );
      console.log( "Blob:", blobOutput );
      
      var linky = document.getElementById( "download_file" );
      linky.href = URL.createObjectURL( blobOutput );
      linky.download = "cluedata.csv";
      
      $( "#download_file" ).fadeIn( "fast", function() { } );
      
      console.log( "Linky:", linky );      
    }, false );
    
    if ( file )
    {
      reader.readAsText( file );
    }
    
  } );
  
} );
